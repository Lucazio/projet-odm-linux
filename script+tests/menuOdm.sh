#!/bin/bash

rep=0
while [ $rep -ne 7 ]
do
	echo "                    MENU"
	echo "                                      Dossier courant: $PWD"
	echo "1 - Remonter d'un dossier"
	echo "2 - Descendre dans un dossier"
	echo "3 - Consulter le contenu du dossier"
	echo "4 - Editer un document"
	echo "5 - Supprimer un document"
	echo "6 - Lancer OpenDroneMap dans ce dossier"
	echo "7 - Quitter"
	rep=0
	while [ $rep -lt 1 ] || [ $rep -gt 7 ]
	do
		read rep
		if [ $rep -lt 1 ] || [ $rep -gt 7 ]
			then
			echo "Il faut entrer une réponse entre 1 et 7 ..."
			sleep 3
		fi
	done
	case $rep in
	1)	cd ..
		;;
	2)	echo " voici les dossiers: "
		for dossier in *
		do
			if [ -d $dossier ]
			then
				echo -n "[$dossier] "
			fi
		done
		echo
		echo
		echo " dans quel dossier voulez vous aller ?"
		dir=""
		read dir
		if [ -d $dir ]
		then
			if [ -x $dir ]
			then
				cd $dir
			else
				echo "Le dossier n'est pas accessible"
			fi
		else 
			echo "Le dossier n'existe pas"
		fi
		;;
	3)	echo "voici les fichiers dans ce répertoire: "
		echo
		for fichier in *
		do
			if [ -d $fichier ]
			then
				echo -n "[$fichier] "
			else
				echo -n "$fichier "
			fi
		done
		sleep 4
		;;
	# 4
	4)	echo "voici les fichiers dans le dossier: "
		echo
		for fichier in *
		do
			if [ -f $fichier ]
			then
				echo -n "$fichier "
			fi
		done
		echo
		echo
		echo "quel fichier voulez vous éditer ?"
		fic=""
		read fic
		gedit $fic
		;;
	5)
		echo "voici les fichiers dans le dossier: "
		echo
		for fichier in *
		do
			if [ -f $fichier ]
			then
				echo -n "$fichier "
			fi
		done
		echo
		echo
		echo "quel fichier voulez vous supprimer ?"
		fic=""
		read fic
			if [ -f $fic ]
			then
				if [ -w $fic ]
				then
					echo "êtes vous sûr de vouloir supprimer $fic ?"
					echo "tapez oui pour le supprimer sinon tapez autre chose"
					valid=""
					read valid
					if [ $valid = "oui" ]
					then
						rm $fic
						echo "le fichier $fic a été supprimé"
					else
						echo "le dossier $fic n'a pas été supprimé"
					fi
			else
				echo "vous ne pouvez pas supprimer ce fichier"
			fi
		else
			echo "le fichier $fic n'est pas un fichier ou n'existe pas"
		fi
		sleep 3
		;;
	6)
		echo "voulez vous vraiment lancer ODM ici: $PWD"/images"?: tapez oui ou non."
		reponse=""
		read reponse
		if [ $reponse = "oui" ]
		then
			boolcheck=false
			for dossier in *
			do
				if [ -d $dossier ]
				then
					if [ $dossier = "images" ]
					then
						boolcheck=true
					fi
				fi
			done
			if [ $boolcheck = true ]
			then
				echo "travail en cours, veuillez patienter."
				CHEMIN=$PWD
				CHEMINDEB=${CHEMIN%/*}
				FINFIN=${CHEMIN##*/}
				FINDEB="/${CHEMINDEB##*/}"
				CHEMINDEB="$CHEMINDEB":"$FINDEB"
				docker run -ti --rm -v $CHEMINDEB opendronemap/odm --project-path $FINDEB $FINFIN
			else
				echo "le dossier image dans $PWD ne semble pas exister"
			fi
		fi
		sleep 3
		;;
	esac
done
